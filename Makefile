SOURCES = v4l-jpeg.c
OBJECTS := $(SOURCES:.c=.o)
EXEC = v4l-jpeg
INSTALL_DIR = /opt/v4l-jpeg
CFLAGS = -Wall -g $(shell pkg-config --cflags libv4l2)
LIBS = $(shell pkg-config --libs libv4l2)

all: $(EXEC)

.c.o:
	$(CC) -c $(CFLAGS) $(INCLUDES) $<

$(EXEC): $(OBJECTS)
	$(CC) -o $(EXEC) $(OBJECTS) $(LIBS)

install:
	install -d $(DESTDIR)$(INSTALL_DIR)
	install -m 664 *.raw $(DESTDIR)$(INSTALL_DIR)
	install -m 775 test.sh v4l-jpeg $(DESTDIR)$(INSTALL_DIR)

clean:
	rm -f *.o $(EXEC)

.PHONY: clean all install
